---------------------------------------------------------------------------------------------------------------------
-- Returns a single value - path of the default data directory of the SQL Server instance on which it is executed. --
---------------------------------------------------------------------------------------------------------------------

SET NOCOUNT ON

SELECT SUBSTRING(physical_name, 1, CHARINDEX(N'master.mdf', LOWER(physical_name)) - 1)
FROM master.sys.master_files
WHERE database_id = 1 AND file_id = 1

SET NOCOUNT OFF