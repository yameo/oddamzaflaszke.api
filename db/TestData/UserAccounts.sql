SET IDENTITY_INSERT [dbo].[User] ON 
GO
-- password: user1
INSERT [dbo].[User] ([Id], [Username], [Password], [Email]) VALUES (1, N'user1', N'l8P6NxduLe0OqhcBUy4nfow7VmS96YlX_Yp9YLWa2Ng+sG9h+dwcY60HpHjV482Cn', N'user1@yameo.pl')
GO
-- password: user2
INSERT [dbo].[User] ([Id], [Username], [Password], [Email]) VALUES (2, N'user2', N'zEV6tAnB3QPlwHib4kzmjvZfX8oXJgN2_snSSXW8iNrfa9O6t2f2WO82x3QDs/HZF', N'user2@yameo.pl')
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
