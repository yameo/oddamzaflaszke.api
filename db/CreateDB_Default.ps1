#
# Script params and start routines
#
Param([string] $server = ".",
      [string] $databaseName = "OddamZaFlaszke", 
      [string] $databaseDir = "",
      [string] $logDir = "")


#
# Script functions
#
# Gets the default data directory of a SQL Server instance.
function GetDefaultDataDir
{
	Param([string] $srv)
	
	$res = `sqlcmd -b -E -S $srv -i .\get_default_data_dir.sql`
	return $res[2].Trim();
}

function GetDataDirOrDefault
{
	Param([string] $srv, [string] $dir)
	
	if ($dir.Trim().Length -eq 0) { return GetDefaultDataDir $srv } else { return $dir }
}

function PrepareDatabaseScript
{
  Param([string] $script)

  # replace all database name templates
  $databaseNameRegex = new-object System.Text.RegularExpressions.Regex -argumentList "<DatabaseName,,.*>", Multiline
  $script = $databaseNameRegex.Replace($script, $databaseName);

  # replace all database dir templates
  $databaseDirRegex = new-object System.Text.RegularExpressions.Regex -argumentList "<DatabaseDir,,.*>", Multiline
  $script = $databaseDirRegex.Replace($script, $databaseDir);

  # replace all log dir templates
  $logDirRegex = new-object System.Text.RegularExpressions.Regex -argumentList "<LogDir,,.*>", Multiline
  $script = $logDirRegex.Replace($script, $logDir);

  return $script;
}

function PrepareSqlcmdBatchScript
{
	Param([string] $currentDir, [object] $filesList, [string] $lastScriptTempFile)
	
	$scriptsBatch = ""	
	foreach ($file in $filesList)
	{	
		$scriptsBatch += ":!! echo `"$file`" > `"$lastScriptTempFile`""
		$scriptsBatch += "`nGO`n"
		$scriptsBatch += ":r `"" + [System.IO.Path]::Combine($currentDir, $file) + "`""
		$scriptsBatch += "`nGO`n"
	}
	
	return $scriptsBatch
}

function Fail
{
	Param([string] $command,
	      [object] $output)
	throw "Database creation failed.`n`n`nThe command was:`n$command`n`n`nThe output was:`n$output`n"
}

# debugging (0=disabled, 1=dump commands, 2=dump commands, variable assignments and function calls)
Set-PSDebug -trace 0

$lastCommandOutput = "";

# count operations to execute
$currentDir = get-location
$objectScripts = [System.IO.File]::ReadAllLines([System.IO.Path]::Combine($currentDir, "CreationOrder_Migrations.txt"))
$defaultDataScripts = [System.IO.File]::ReadAllLines([System.IO.Path]::Combine($currentDir, "CreationOrder_DefaultData.txt"))
$testDataScripts = [System.IO.File]::ReadAllLines([System.IO.Path]::Combine($currentDir, "CreationOrder_TestData.txt"))

$currentOp = 0
$totalOps = 9

# 0. prepare scripts
Write-Progress -Activity ("Creating database " + $databaseName) -currentOperation "Preparing scripts: setting data dirs" -Status "Step $($currentOp+1)/$totalOps" -percentComplete ($currentOp/$totalOps*100)
$currentOp++
 
$databaseDir = GetDataDirOrDefault $server $databaseDir
$logDir = GetDataDirOrDefault $server $logDir

# 1. create database
Write-Progress -Activity ("Creating database " + $databaseName) -currentOperation "create_database.sql" -Status "Step $($currentOp+1)/$totalOps" -percentComplete ($currentOp/$totalOps*100)
$currentOp++

$content = [System.IO.File]::ReadAllText([System.IO.Path]::Combine($currentDir, "create_database.sql"))
$content = PrepareDatabaseScript($content)
$tempFile = [System.IO.Path]::GetTempFileName()

set-content $tempFile $content
$lastCommandOutput = sqlcmd -b -E -S $server -i $tempFile 2>&1
if (!$?) { Fail "sqlcmd -b -E -S $server -i $tempFile" $lastCommandOutput }

[System.IO.File]::Delete($tempFile)


# 2. create database objects
Write-Progress -Activity ("Creating database " + $databaseName)  -currentOperation "Reading scripts contents" -Status "Step $($currentOp+1)/$totalOps" -percentComplete ($currentOp/$totalOps*100)
$currentOp++

$lastScriptTempFile = [System.IO.Path]::GetTempFileName()
$scriptsBatch = PrepareSqlcmdBatchScript $currentDir $objectScripts $lastScriptTempFile

# 3.
Write-Progress -Activity ("Creating database " + $databaseName) -currentOperation "Creating database objects" -Status "Step $($currentOp+1)/$totalOps" -percentComplete ($currentOp/$totalOps*100)
$currentOp++

$lastCommandOutput = echo $scriptsBatch | sqlcmd -b -E -S $server -d $databaseName 2>&1
if (!$?) {
	$output= "Error in " + [System.IO.File]::ReadAllText($lastScriptTempFile) + ": " + $lastCommandOutput	
	[System.IO.File]::Delete($lastScriptTempFile)
	
	Fail "sqlcmd -b -E -S $server -d $databaseName" $output
}

[System.IO.File]::Delete($lastScriptTempFile)

# 4. create default data
Write-Progress -Activity ("Creating database " + $databaseName)  -currentOperation "Reading scripts contents" -Status "Step $($currentOp+1)/$totalOps" -percentComplete ($currentOp/$totalOps*100)
$currentOp++

$lastScriptTempFile = [System.IO.Path]::GetTempFileName()
$scriptsBatch = PrepareSqlcmdBatchScript $currentDir $defaultDataScripts $lastScriptTempFile

# 5.
Write-Progress -Activity ("Creating database " + $databaseName) -currentOperation "Creating default data" -Status "Step $($currentOp+1)/$totalOps" -percentComplete ($currentOp/$totalOps*100)
$currentOp++

$lastCommandOutput = echo $scriptsBatch | sqlcmd -b -E -S $server -d $databaseName 2>&1
if (!$?) {
	$output= "Error in " + [System.IO.File]::ReadAllText($lastScriptTempFile) + ": " + $lastCommandOutput	
	[System.IO.File]::Delete($lastScriptTempFile)
	
	Fail "sqlcmd -b -E -S $server -d $databaseName" $output
}

[System.IO.File]::Delete($lastScriptTempFile)

# 6. create test data
Write-Progress -Activity ("Creating database " + $databaseName)  -currentOperation "Reading scripts contents" -Status "Step $($currentOp+1)/$totalOps" -percentComplete ($currentOp/$totalOps*100)
$currentOp++

$lastScriptTempFile = [System.IO.Path]::GetTempFileName()
$scriptsBatch = PrepareSqlcmdBatchScript $currentDir $testDataScripts $lastScriptTempFile

# 7.
Write-Progress -Activity ("Creating database " + $databaseName) -currentOperation "Creating test data" -Status "Step $($currentOp+1)/$totalOps" -percentComplete ($currentOp/$totalOps*100)
$currentOp++

$lastCommandOutput = echo $scriptsBatch | sqlcmd -b -E -S $server -d $databaseName 2>&1
if (!$?) {
	$output= "Error in " + [System.IO.File]::ReadAllText($lastScriptTempFile) + ": " + $lastCommandOutput	
	[System.IO.File]::Delete($lastScriptTempFile)
	
	Fail "sqlcmd -b -E -S $server -d $databaseName" $output
}

[System.IO.File]::Delete($lastScriptTempFile)

# 4. grant permissions to the database
Write-Progress -Activity "Applying security settings" -currentOperation "Permissions" -Status "Step $($currentOp+1)/$totalOps" -percentComplete ($currentOp/$totalOps*100)
$currentOp++
$scriptFile = [System.IO.Path]::Combine($currentDir, "grant_permissions.sql")

$lastCommandOutput = sqlcmd -b -E -S $server -d $databaseName -i $scriptFile 2>&1
#if (!$?) { Fail "sqlcmd -b -E -S $server -d $databaseName -i $scriptFile" $lastCommandOutput } # this one may fail, ignore it (or fix grant_toi_permissions.sql not to fail on SQL Express)


# disable debugging now
Set-PSDebug -trace 0