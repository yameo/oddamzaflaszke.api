USE [master]
GO

SET NOCOUNT ON;

IF EXISTS (SELECT TOP(1) [Database_id] 
		   FROM [Sys].[Databases] 
		   WHERE [Name] = '<DatabaseName,,OddamZaFlaszke>')
BEGIN
	-- close all connections
	ALTER DATABASE <DatabaseName,,OddamZaFlaszke> SET OFFLINE WITH ROLLBACK IMMEDIATE;
	ALTER DATABASE <DatabaseName,,OddamZaFlaszke> SET ONLINE;

	-- drop database
	DROP DATABASE <DatabaseName,,OddamZaFlaszke>;
END;
GO

-- create database
CREATE DATABASE [<DatabaseName,,OddamZaFlaszke>] 
ON  PRIMARY (NAME = N'PRIMARY', 
			 FILENAME = N'<DatabaseDir,,C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Data\><DatabaseName,,OddamZaFlaszke>_PRIMARY.MDF', 
			 SIZE = 10MB , 
			 MAXSIZE = UNLIMITED, 
			 FILEGROWTH = 1MB)
LOG ON (NAME = N'TRANSACTION_LOG', 
		FILENAME = N'<LogDir,,C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\Data\><DatabaseName,,OddamZaFlaszke>_LOG.LDF', 
		SIZE = 1MB, 
		MAXSIZE = 2GB, 
		FILEGROWTH = 10%)
COLLATE SQL_Latin1_General_CP1_CI_AS;
