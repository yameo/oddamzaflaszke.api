﻿namespace OddamZaFlaszke.Services
{
	public interface IPasswordService
	{
		string EncryptPassword(string password);
		bool IsPasswordValid(string password, string passwordHash);
	}
}