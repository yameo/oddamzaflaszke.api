﻿using System;

namespace OddamZaFlaszke.Services
{
	public interface ILoggerService
	{
		void Debug(string message, params object[] args);
		void Error(Exception exception, string message, params object[] args);
		void Info(string message, params object[] args);
		void Warn(string message, params object[] args);
		bool IsTraceEnabled();
	}
}