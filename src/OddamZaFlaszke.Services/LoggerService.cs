﻿using System;
using NLog;

namespace OddamZaFlaszke.Services
{
	public class LoggerService : ILoggerService
	{
		private readonly Logger _log;

		public LoggerService(Type callerType)
		{
			_log = LogManager.GetLogger(callerType.FullName);
		}

		public LoggerService(string callerName)
		{
			_log = LogManager.GetLogger(callerName);
		}

		public void Info(string message, params object[] args)
		{
			_log.Info(message, args);
		}

		public void Debug(string message, params object[] args)
		{
			_log.Debug(message, args);
		}

		public void Warn(string message, params object[] args)
		{
			_log.Warn(message, args);
		}

		public void Error(Exception exception, string message, params object[] args)
		{
			_log.Error(exception, message, args);
		}

		public bool IsTraceEnabled()
		{
			return _log.IsTraceEnabled;
		}
	}
}