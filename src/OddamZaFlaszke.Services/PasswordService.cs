﻿using System;
using System.Security.Cryptography;

namespace OddamZaFlaszke.Services
{
    public class PasswordService : IPasswordService
	{
		private readonly int _saltBytesSize;
		private readonly int _hashBytesSize;
		private readonly int _hasingIterationsCount;

		public PasswordService()
			: this(24, 24, 1000)
		{
		}

		public PasswordService(int saltBytesSize, int hashBytesSize, int hasingIterationsCount)
		{
			_saltBytesSize = saltBytesSize;
			_hashBytesSize = hashBytesSize;
			_hasingIterationsCount = hasingIterationsCount;
		}

		public string EncryptPassword(string password)
		{
			var salt = GenerateSalt();
			var saltBase64 = Convert.ToBase64String(salt);

			var passwordHash = ComputeHash(password, salt);

			return $"{saltBase64}_{Convert.ToBase64String(passwordHash)}";
		}

		public bool IsPasswordValid(string password, string passwordHash)
		{
			var passwordElements = passwordHash.Split('_');

			var saltBase64 = passwordElements[0];
			var hashBase64 = passwordElements[1];

			var salt = Convert.FromBase64String(saltBase64);
			var hash = Convert.ToBase64String(ComputeHash(password, salt));

			return hashBase64 == hash;
		}

		internal byte[] GenerateSalt()
		{
			using (var saltGenerator = new RNGCryptoServiceProvider())
			{
				var salt = new byte[_saltBytesSize];
				saltGenerator.GetBytes(salt);
				return salt;
			}
		}

		internal byte[] ComputeHash(string password, byte[] salt)
		{
			using (var hashGenerator = new Rfc2898DeriveBytes(password, salt))
			{
				hashGenerator.IterationCount = _hasingIterationsCount;
				return hashGenerator.GetBytes(_hashBytesSize);
			}
		}
	}
}
