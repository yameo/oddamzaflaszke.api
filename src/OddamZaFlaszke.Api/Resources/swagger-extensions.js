﻿$("#input_apiKey").prop("placeholder", "username:password");

$("#input_apiKey").change(function () {
	alert("Login in progress. Please wait for information about login status.");

	var key = $("#input_apiKey")[0].value;
	var credentials = key.split(":");
	var url = window.location.toString();
	url = url.replace(/docs\/index.*/, "") + "oauth/token";

	$.ajax({
		url: url,
		type: "post",
		contenttype: "x-www-form-urlencoded",
		data: "grant_type=password&username=" + credentials[0] + "&password=" + credentials[1],
		success: function (response) {
			var bearerToken = "Bearer " + response.access_token;
			var apiKeyAuth = new SwaggerClient.ApiKeyAuthorization("Authorization", bearerToken, "header");

			window.swaggerUi.api.clientAuthorizations.remove("api_key");
			window.swaggerUi.api.clientAuthorizations.add("oauth2", apiKeyAuth);

			alert("Login success");
		},
		error: function() {
			alert("Login failed!");
		}
	});
});
