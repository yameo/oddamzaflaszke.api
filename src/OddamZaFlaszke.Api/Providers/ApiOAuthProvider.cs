﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using OddamZaFlaszke.Api.Services;

namespace OddamZaFlaszke.Api.Providers
{
	public class ApiOAuthProvider : OAuthAuthorizationServerProvider
	{
		private readonly string _publicClientId;
		private readonly IAuthenticationService _authenticationService;

		public ApiOAuthProvider(string publicClientId, IAuthenticationService authenticationService)
		{
			_publicClientId = publicClientId ?? throw new ArgumentNullException(nameof(publicClientId));
			_authenticationService = authenticationService;
		}

		public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
		{
			var isUserValid = _authenticationService.IsUserValid(
				context.UserName,
				context.Password);

			if (!isUserValid)
			{
				context.SetError("invalid_grant", "The user name or password is incorrect.");
				return Task.FromResult(false);
			}

			var oAuthIdentity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);
			oAuthIdentity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));			

			var properties = CreateProperties(context.UserName);
			var ticket = new AuthenticationTicket(oAuthIdentity, properties);
			context.Validated(ticket);

			return Task.FromResult(true);
		}

		public override Task TokenEndpoint(OAuthTokenEndpointContext context)
		{
			foreach (var property in context.Properties.Dictionary)
			{
				context.AdditionalResponseParameters.Add(property.Key, property.Value);
			}

			return Task.FromResult<object>(null);
		}

		public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
		{			
			if (context.ClientId == null)
			{
				context.Validated();
			}

			return Task.FromResult<object>(null);
		}

		public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
		{
			if (context.ClientId != _publicClientId)
			{
				return Task.FromResult<object>(null);
			}

			var expectedRootUri = new Uri(context.Request.Uri, "/");

			if (expectedRootUri.AbsoluteUri == context.RedirectUri)
			{
				context.Validated();
			}

			return Task.FromResult<object>(null);
		}

		public static AuthenticationProperties CreateProperties(string userName)
		{
			IDictionary<string, string> data = new Dictionary<string, string>
			{
				{ "userName", userName }
			};
			return new AuthenticationProperties(data);
		}
	}
}