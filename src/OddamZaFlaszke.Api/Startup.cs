﻿using System;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using OddamZaFlaszke.Api.Middlewares;
using OddamZaFlaszke.Api.Providers;
using OddamZaFlaszke.Api.Services;
using OddamZaFlaszke.Services;
using Owin;

[assembly: OwinStartup(typeof(OddamZaFlaszke.Api.Startup))]

namespace OddamZaFlaszke.Api
{
	public class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			var httpConfiguration = new HttpConfiguration();
			
			WebApiConfig.Register(httpConfiguration);
			DependencyInjectionConfig.Register(app, httpConfiguration);
			SwaggerConfig.Register(httpConfiguration);

			var oAuthOptions = new OAuthAuthorizationServerOptions
			{
				TokenEndpointPath = new PathString("/oauth/token"),
				Provider = new ApiOAuthProvider(
					"OddamZaFlaszke.FrontEnd",
					(IAuthenticationService)httpConfiguration
						.DependencyResolver
						.GetService(typeof(IAuthenticationService))),
				AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
				AllowInsecureHttp = true
			};

			app.UseCors(CorsOptions.AllowAll);

			app.Use<RequestLogMiddleware>(new LoggerService("Api"));

			app.UseOAuthAuthorizationServer(oAuthOptions);
			app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

			app.UseWebApi(httpConfiguration);
		}
	}
}
