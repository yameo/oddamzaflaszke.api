﻿using System.Net;
using System.Web.Http;
using OddamZaFlaszke.Api.Models;
using OddamZaFlaszke.Api.Services;

namespace OddamZaFlaszke.Api.Controllers
{
	[RoutePrefix("accounts")]
	public class AccountsController : ApiController
	{
		private readonly IAuthenticationService _authenticationService;

		public AccountsController(IAuthenticationService authenticationService)
		{
			_authenticationService = authenticationService;
		}

		/// <summary>
		/// Register an account
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		[HttpPost]
		[Route]
		public IHttpActionResult Register(UserModel model)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}

			_authenticationService.Register(model);

			return StatusCode(HttpStatusCode.NoContent);
		}
	}
}