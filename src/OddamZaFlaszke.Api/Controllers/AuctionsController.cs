﻿using System.Collections.Generic;
using System.Web.Http;
using OddamZaFlaszke.Api.Models;
using OddamZaFlaszke.Api.Services;
using System.Net;

namespace OddamZaFlaszke.Api.Controllers
{
	[Authorize]
	[RoutePrefix("auctions")]
	public class AuctionsController : BaseApiController
	{
		private readonly IAuctionService _auctionService;

		/// <param name="auctionService"></param>
		public AuctionsController(IAuctionService auctionService)
		{
			_auctionService = auctionService;
		}

		/// <summary>
		/// Get auctions created by an user
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Route("my")]
		public IEnumerable<AuctionDescriptionModel> GetUserAuctions()
		{
			return _auctionService.GetUserAuctions(ClaimUsername);
		}

		/// <summary>
		/// Get public auctions
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Route("public")]
		public IEnumerable<AuctionDescriptionModel> GetPublicAuctions()
		{
			return _auctionService.GetPublicAuctionsForUser(ClaimUsername);
		}

		/// <summary>
		/// Get auction details
		/// </summary>
		/// <param name="id">Auction id</param>
		/// <returns></returns>
		[HttpGet]
		[Route("{id}")]
		public IHttpActionResult GetAuction(int id)
		{
			var model = _auctionService.GetAuction(id);
			if (model == null)
			{
				return NotFound();
			}

			return Content(HttpStatusCode.OK, model);
		}

		/// <summary>
		/// Add an auction
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		[HttpPost]
		[Route]
		public IHttpActionResult AddAuction(AuctionCreateModel model)
		{
			var newAuction = _auctionService.AddAuction(ClaimUsername, model);

			return Created($"{Request.RequestUri}/{newAuction.Id}", newAuction);
		}

		/// <summary>
		/// Update an auction
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		[HttpPut]
		[Route]
		public IHttpActionResult UpdateAuction(AuctionCreateModel model)
		{
			return StatusCode(HttpStatusCode.NotImplemented);
		}

		/// <summary>
		/// Add an offer
		/// </summary>
		/// <param name="auctionId">Auction Id</param>
		/// <param name="model">Offer model</param>
		/// <returns></returns>
		[HttpPost]
		[Route("{auctionId}/offers")]
		public IHttpActionResult AddOffer(int auctionId, OfferCreateModel model)
		{
			var newOffer = _auctionService.AddOffer(ClaimUsername, auctionId, model);

			return Created($"{Request.RequestUri}/{newOffer.Id}", newOffer);
		}

		/// <summary>
		/// Close an auction
		/// </summary>
		/// <param name="id">Auction Id</param>
		/// <param name="model">Auction closing model</param>
		/// <returns></returns>
		[HttpDelete]
		[Route("{id}")]
		public IHttpActionResult CloseAuction(int id, AuctionClosingModel model)
		{
			var closeModel = model.SelectedOfferId.HasValue
				? _auctionService.CloseAuction(ClaimUsername, id, model.SelectedOfferId.Value)
				: _auctionService.CloseAuction(ClaimUsername, id);

			if (closeModel == null)
			{
				return NotFound();
			}

			return Content(HttpStatusCode.OK, closeModel);
		}
	}
}