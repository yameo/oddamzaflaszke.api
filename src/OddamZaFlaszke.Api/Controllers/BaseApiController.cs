﻿using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace OddamZaFlaszke.Api.Controllers
{
	public class BaseApiController : ApiController
	{
		private string _username;

		/// <summary>
		/// Claim ID
		/// </summary>
		protected string ClaimUsername
		{
			get
			{
				if (string.IsNullOrWhiteSpace(_username))
				{
					_username = GetClaimUsername();
				}

				return _username;
			}
		}

		private string GetClaimUsername()
		{
			var principal = Request.GetRequestContext()?.Principal as ClaimsPrincipal;

			return principal?.Claims
				.FirstOrDefault(i => i.Type == ClaimTypes.Name.ToString())?.Value;

		}
	}
}