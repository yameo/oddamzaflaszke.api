﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using OddamZaFlaszke.Api.Services;
using OddamZaFlaszke.Repository.Repositories;
using OddamZaFlaszke.Services;
using Owin;

namespace OddamZaFlaszke.Api
{
	public static class DependencyInjectionConfig
	{
		public static void Register(IAppBuilder app, HttpConfiguration config)
		{
			var builder = new ContainerBuilder();
			builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
			builder.RegisterType<PasswordService>().AsImplementedInterfaces();
			builder.RegisterType<UserRepository>().AsImplementedInterfaces();
			builder.RegisterType<UnitOfWork>().AsImplementedInterfaces();
			builder.RegisterType<AuthenticationService>().AsImplementedInterfaces();
			builder.RegisterType<AuctionService>().AsImplementedInterfaces();

			var container = builder.Build();
			config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

			app.UseAutofacMiddleware(container);
			app.UseAutofacWebApi(config);
		}
	}
}