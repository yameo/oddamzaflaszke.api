using System;
using System.Web;
using System.Web.Http;
using Swashbuckle.Application;

namespace OddamZaFlaszke.Api
{
    public class SwaggerConfig
    {
        public static void Register(HttpConfiguration config)
        {
	        var thisAssembly = typeof(SwaggerConfig).Assembly;

			config
				.EnableSwagger("docs/ver/{apiVersion}", c =>
				{
					c.RootUrl(req => req.RequestUri.GetLeftPart(UriPartial.Authority) + VirtualPathUtility.ToAbsolute("~/").TrimEnd('/'));
					c.SingleApiVersion("v1", "Oddam Za Flaszke API");
					c.IncludeXmlComments(GetPortalXmlCommentsPath());					
				})
				.EnableSwaggerUi("docs/{*assetPath}", c =>
				{
					c.DisableValidator();
					c.InjectJavaScript(thisAssembly, "OddamZaFlaszke.Api.Resources.swagger-extensions.js");
				});
		}

		private static string GetPortalXmlCommentsPath()
		{
			return $@"{AppDomain.CurrentDomain.BaseDirectory}\bin\OddamZaFlaszke.Api.xml";
		}
	}
}
