﻿using OddamZaFlaszke.Api.Models;

namespace OddamZaFlaszke.Api.Services
{
	public interface IAuthenticationService
	{
		bool IsUserValid(string username, string password);

		void Register(UserModel model);
	}
}