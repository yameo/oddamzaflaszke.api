﻿using OddamZaFlaszke.Api.Models;
using OddamZaFlaszke.Repository.Entities;
using OddamZaFlaszke.Repository.Repositories;
using OddamZaFlaszke.Services;

namespace OddamZaFlaszke.Api.Services
{
	public class AuthenticationService : IAuthenticationService
	{
		private readonly IUserRepository _userRepository;
		private readonly IPasswordService _passwordService;

		public AuthenticationService(
			IUserRepository userRepository,
			IPasswordService passwordService)
		{
			_userRepository = userRepository;
			_passwordService = passwordService;
		}

		public bool IsUserValid(string username, string password)
		{
			var user = _userRepository.Find(username);
			return user != null && _passwordService.IsPasswordValid(password, user.Password);
		}

		public void Register(UserModel model)
		{
			var user = new User
			{
				Username = model.Username,
				Email = model.Email,
				Password = _passwordService.EncryptPassword(model.Password)
			};

			_userRepository.Add(user);
			_userRepository.Commit();
		}
	}
}