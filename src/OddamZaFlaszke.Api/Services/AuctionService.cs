﻿using System;
using System.Collections.Generic;
using System.Linq;
using OddamZaFlaszke.Api.Models;
using OddamZaFlaszke.Repository.Entities;
using OddamZaFlaszke.Repository.Repositories;

namespace OddamZaFlaszke.Api.Services
{
	public class AuctionService : IAuctionService
	{		
		private readonly IUserRepository _userRepository;
		private readonly IAuctionRepository _auctionRepository;
		private readonly IOfferRepository _offerRepository;

		public AuctionService(IUnitOfWork unitOfWork)
		{
			_userRepository = unitOfWork.UserRepository;
			_auctionRepository = unitOfWork.AuctionRepository;
			_offerRepository = unitOfWork.OfferRepository;
		}

		public IEnumerable<AuctionDescriptionModel> GetUserAuctions(string username)
		{
			return _auctionRepository.Find(username)
				.ToList()
				.Select(i => new AuctionDescriptionModel(i))
				.AsEnumerable();
		}

		public IEnumerable<AuctionDescriptionModel> GetPublicAuctionsForUser(string username)
		{
			var now = DateTime.Now;
			
			return _auctionRepository.GetAll(i => i.User)
				.Where(i => i.EndDate > now 
					&& !i.CloseDate.HasValue
					&& i.User.Username != username)
				.ToList()
				.Select(i => new AuctionDescriptionModel(i))
				.AsEnumerable();
		}

		public AuctionDetailsModel GetAuction(int id)
		{
			var entity = _auctionRepository.Find(id, i => i.User, i => i.Offers);

			return entity != null ? new AuctionDetailsModel(entity) : null;
		}

		public AuctionDescriptionModel AddAuction(
			string username,
			AuctionCreateModel model)
		{
			var user = _userRepository.Find(username);
			if (user == null)
			{
				return null;
			}

			var auction = new Auction
			{
				User = user,
				CreationDate = DateTime.Now,
				EndDate = model.EndDate,
				Description = model.Description
			};

			_auctionRepository.Add(auction);
			_auctionRepository.Commit();

			return new AuctionDescriptionModel(auction);
		}

		public OfferDescriptionModel AddOffer(
			string username,
			int auctionId,
			OfferCreateModel model)
		{
			var user = _userRepository.Find(username);
			if (user == null)
			{
				return null;
			}

			var offer = new Offer
			{
				AuctionId = auctionId,
				User = user,
				CreationDate = DateTime.Now,
				CurrencyType = model.CurrencyType,
				Quantity = model.Quantity,
				Comment = model.Comment
			};

			_offerRepository.Add(offer);
			_offerRepository.Commit();

			return new OfferDescriptionModel(offer);
		}

		public AuctionClosedModel CloseAuction(string username, int auctionId)
		{
			return CloseAuction(username, auctionId, 0);
		}

		public AuctionClosedModel CloseAuction(
			string username,
			int auctionId,
			int offerId)
		{
			var auction = _auctionRepository
				.GetAll()
				.FirstOrDefault(i => i.Id == auctionId
					&& i.User.Username == username);

			//Not found
			if (auction == null)
			{
				return null;
			}

			//Already closed
			if (auction.CloseDate.HasValue)
			{
				return null;
			}

			if (offerId > 0)
			{
				var offer = auction.Offers.FirstOrDefault(i => i.Id == offerId);
				if (offer != null)
				{
					offer.Selected = true;
				}
			}

			auction.CloseDate = DateTime.Now;

			_auctionRepository.Commit();

			return new AuctionClosedModel
			{
				CloseDate = auction.CloseDate,
				SelectedOfferId = offerId > 0 ? offerId : (int?)null
			};
		}
	}
}