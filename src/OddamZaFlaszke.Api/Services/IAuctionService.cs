﻿using System.Collections.Generic;
using OddamZaFlaszke.Api.Models;

namespace OddamZaFlaszke.Api.Services
{
	public interface IAuctionService
	{		
		IEnumerable<AuctionDescriptionModel> GetPublicAuctionsForUser(string username);

		IEnumerable<AuctionDescriptionModel> GetUserAuctions(string username);

		AuctionDetailsModel GetAuction(int id);

		AuctionDescriptionModel AddAuction(string username, AuctionCreateModel model);

		OfferDescriptionModel AddOffer(string username, int auctionId, OfferCreateModel model);

		AuctionClosedModel CloseAuction(string username, int auctionId);

		AuctionClosedModel CloseAuction(string username, int auctionId, int offerId);
	}
}