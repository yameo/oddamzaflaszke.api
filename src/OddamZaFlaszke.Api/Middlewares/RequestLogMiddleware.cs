﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Owin;
using OddamZaFlaszke.Services;

namespace OddamZaFlaszke.Api.Middlewares
{
	public class RequestLogMiddleware : OwinMiddleware
	{
		private readonly ILoggerService _logger;

		public RequestLogMiddleware(
			OwinMiddleware next,
			ILoggerService logger)
			: base(next)
		{
			_logger = logger;			
		}

		public override async Task Invoke(IOwinContext context)
		{
			var request = context.Request;

			var sw = new Stopwatch();
			sw.Start();

			await Next.Invoke(context);

			_logger.Debug("{0} {1} ({2}ms)", request.Method, request.Uri.PathAndQuery, sw.Elapsed.Milliseconds);
			sw.Stop();
		}
	}
}
