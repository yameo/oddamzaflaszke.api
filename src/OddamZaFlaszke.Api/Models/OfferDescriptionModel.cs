﻿using System;
using OddamZaFlaszke.Repository.Entities;
using OddamZaFlaszke.Repository.Enums;

namespace OddamZaFlaszke.Api.Models
{
	public class OfferDescriptionModel
	{
		public OfferDescriptionModel()
		{
		}

		internal OfferDescriptionModel(Offer entity)
		{
			Id = entity.Id;
			AuctionId = entity.AuctionId;
			Owner = entity.User.Username;
			Quantity = entity.Quantity;
			Selected = entity.Selected;
			CreationDate = entity.CreationDate;
			CurrencyType = entity.CurrencyType;
			Comment = entity.Comment;
		}

		public int Id { get; set; }

		public int AuctionId { get; set; }

		public string Owner { get; set; }

		public int Quantity { get; set; }

		public bool Selected { get; set; }

		public CurrencyType CurrencyType { get; set; }

		public DateTime CreationDate { get; set; }

		public string Comment { get; set; }
	}
}