﻿using System.ComponentModel.DataAnnotations;
using OddamZaFlaszke.Repository.Enums;

namespace OddamZaFlaszke.Api.Models
{
	public class OfferCreateModel
	{
		[Required]
		public int Quantity { get; set; }

		[Required]
		public CurrencyType CurrencyType { get; set; }

		[Required]
		public string Comment { get; set; }
	}
}