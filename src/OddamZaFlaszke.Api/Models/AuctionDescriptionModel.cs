﻿using OddamZaFlaszke.Repository.Entities;
using System;

namespace OddamZaFlaszke.Api.Models
{
	public class AuctionDescriptionModel
	{
		public AuctionDescriptionModel()
		{
		}

		internal AuctionDescriptionModel(Auction entity)
		{
			Id = entity.Id;
			Description = entity.Description;
			CreationDate = entity.CreationDate;
			EndDate = entity.EndDate;
			CloseDate = entity.CloseDate;
			Owner = entity.User.Username;
		}

		public int Id { get; set; }

		public string Owner { get; set; }

		public string Description { get; set; }

		public DateTime CreationDate { get; set; }

		public DateTime EndDate { get; set; }

		public DateTime? CloseDate { get; set; }
	}
}