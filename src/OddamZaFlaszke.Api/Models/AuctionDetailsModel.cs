﻿using OddamZaFlaszke.Repository.Entities;
using System.Collections.Generic;
using System.Linq;

namespace OddamZaFlaszke.Api.Models
{
	public class AuctionDetailsModel
	{
		public AuctionDetailsModel()
		{
		}

		internal AuctionDetailsModel(Auction entity)
		{
			Auction = new AuctionDescriptionModel(entity);
			Offers = entity.Offers
				.ToList()
				.Select(i => new OfferDescriptionModel(i));
		}

		public AuctionDescriptionModel Auction { get; set; }

		public IEnumerable<OfferDescriptionModel> Offers { get; set; }
	}
}