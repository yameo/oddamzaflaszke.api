﻿using System.ComponentModel.DataAnnotations;

namespace OddamZaFlaszke.Api.Models
{
	public class UserModel
	{
		[Required]
		[StringLength(50)]
		public string Username { get; set; }

		[Required]
		[StringLength(255)]
		public string Password { get; set; }

		[Required]
		[StringLength(255)]
		public string Email { get; set; }
	}
}