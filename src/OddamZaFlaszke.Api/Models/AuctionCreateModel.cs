﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OddamZaFlaszke.Api.Models
{
	public class AuctionCreateModel
	{
		[Required]
		public string Description { get; set; }

		[Required]
		public DateTime EndDate { get; set; }
	}
}