﻿using System;

namespace OddamZaFlaszke.Api.Models
{
	public class AuctionClosedModel : AuctionClosingModel
	{		
		public DateTime? CloseDate { get; set; }
	}
}