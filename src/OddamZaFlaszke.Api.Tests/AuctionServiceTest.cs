﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FluentAssertions;
using Moq;
using OddamZaFlaszke.Api.Models;
using OddamZaFlaszke.Api.Services;
using Xunit;
using OddamZaFlaszke.Repository.Repositories;
using OddamZaFlaszke.Repository.Entities;
using OddamZaFlaszke.Repository.Enums;

namespace OddamZaFlaszke.Api.Tests
{
	public class AuctionServiceTest
	{
		private readonly IAuctionService _service;
		private Mock<IUnitOfWork> _unitOfWorkMock;
		private List<Auction> _mockAuctions;
		private List<User> _mockUsers;
		private List<Offer> _mockOffers;

		public AuctionServiceTest()
		{
			PrepareUnitOfWorkMock();
			_service = new AuctionService(_unitOfWorkMock.Object);
		}

		[Fact]
		public void UserCanGetOwnedAuctions()
		{
			//Arrange
			const string testUsername = "testUsername";
			_mockAuctions.Add(CreateMockAuction(testUsername));
			_mockAuctions.Add(CreateMockAuction(testUsername));
			_mockAuctions.Add(CreateMockAuction("otherUsername"));

			//Act
			var testUserAuctions = _service.GetUserAuctions(testUsername);

			//Assert
			testUserAuctions.All(i => i.Owner == testUsername)
				.Should()
				.BeTrue();
		}

		[Fact]
		public void UserCanGetPublicAuctions()
		{
			//Arrange
			const string testUsername = "testUsername";
			var endDate = DateTime.Now.AddDays(1);
			_mockAuctions.Add(CreateMockAuction(testUsername));
			_mockAuctions.Add(CreateMockAuction("otherUsername", 1, "description", endDate));

			//Act
			var testPublicAuctions = _service.GetPublicAuctionsForUser(testUsername).ToList();

			//Assert
			testPublicAuctions.Any().Should().BeTrue();
			testPublicAuctions.All(i => i.Owner != testUsername && i.EndDate == endDate)
				.Should()
				.BeTrue();
		}

		[Fact]
		public void UserCanGetAuctionDetails()
		{
			//Arrange
			const string testUsername = "testUsername";
			const string testDescription = "testDescription";
			const int testId = 2;
			_mockAuctions.Add(CreateMockAuction(testUsername, testId, testDescription));

			//Act
			var auctionDetails = _service.GetAuction(testId);

			//Assert
			auctionDetails.Should().NotBeNull();
			auctionDetails.Auction.Id.Should().Be(testId);
			auctionDetails.Auction.Description.Should().Be(testDescription);
			auctionDetails.Auction.Owner.Should().Be(testUsername);
		}

		[Fact]
		public void UserCanAddNewAuction()
		{
			//Arrange
			const string testUsername = "testUsername";
			const string testDescription = "testDescription";
			const int testUserId = 1;
			var endDate = DateTime.Now.AddDays(1);

			_mockUsers.Add(CreateMockUser(testUsername, testUserId));

			var testModel = new AuctionCreateModel
			{
				EndDate = endDate,
				Description = testDescription
			};

			//Act
			var result = _service.AddAuction(testUsername, testModel);
			
			//Assert
			result.Owner.Should().Be(testUsername);
			result.Description.Should().Be(testDescription);
			result.EndDate.Should().Be(endDate);
			_mockAuctions.All(i => i.UserId == testUserId).Should().BeTrue();
		}

		[Fact]
		public void UserCanAddOfferToExistingAuction()
		{
			//Arrange
			const string testAuctionOwner = "testAuctionOwner";
			const string testUsername = "testUsername";
			const string testOfferComment = "testOfferComment";
			const int testOfferQunatity = 10;
			const CurrencyType testOfferCurrencyType = CurrencyType.Beer;

			const int testAuctionId = 1;
			const int testUserId = 1;

			_mockAuctions.Add(CreateMockAuction(testAuctionOwner, testAuctionId, Guid.NewGuid().ToString()));
			_mockUsers.Add(CreateMockUser(testUsername, testUserId));

			var testModel = new OfferCreateModel
			{
				Comment = testOfferComment,
				CurrencyType = testOfferCurrencyType,
				Quantity = testOfferQunatity
			};

			//Act
			var result = _service.AddOffer(testUsername, testAuctionId, testModel);

			//Assert
			result.Should().NotBeNull();
			result.Owner.Should().Be(testUsername);
			result.AuctionId.Should().Be(testAuctionId);
			_mockOffers.All(i => i.UserId == testUserId).Should().BeTrue();
		}

		[Fact]
		public void UserCanCloseOwnedAuction()
		{
			//Arrange
			const string testUsername = "testUsername";
			const int testAuctionId = 1;

			_mockAuctions.Add(CreateMockAuction(testUsername, testAuctionId, Guid.NewGuid().ToString()));

			//Act
			var result = _service.CloseAuction(testUsername, testAuctionId);

			//Assert
			result.Should().NotBeNull();
			result.CloseDate.Should().NotBeNull();
			_mockAuctions.All(i => i.User.Username == testUsername && i.CloseDate.HasValue).Should().BeTrue();
		}

		private void PrepareUnitOfWorkMock()
		{
			_unitOfWorkMock = new Mock<IUnitOfWork>();
			_mockAuctions = new List<Auction>();
			_mockUsers = new List<User>();
			_mockOffers = new List<Offer>();

			var auctionRepositoryMock = new Mock<IAuctionRepository>();
			var userRepositoryMock = new Mock<IUserRepository>();
			var offerRepositoryMock = new Mock<IOfferRepository>();

			auctionRepositoryMock.Setup(i => i.Find(It.IsAny<string>()))
				.Returns<string>(username => _mockAuctions
					.Where(a => a.User.Username == username)
					.AsQueryable());

			auctionRepositoryMock.Setup(i => i.Find(It.IsAny<int>(), It.IsAny<Expression<Func<Auction, object>>[]>()))
				.Returns<int, Expression<Func<Auction, object>>[]>((id, include) => _mockAuctions
					.FirstOrDefault(i => i.Id == id));

			auctionRepositoryMock.Setup(i => i.GetAll(It.IsAny<Expression<Func<Auction, object>>[]>()))
				.Returns<Expression<Func<Auction, object>>[]>(expressions => _mockAuctions.AsQueryable());

			auctionRepositoryMock.Setup(i => i.Add(It.IsAny<Auction>()))
				.Callback<Auction>(i =>
				{
					i.UserId = i.User.Id;
					_mockAuctions.Add(i);
				});

			userRepositoryMock.Setup(i => i.Find(It.IsAny<string>()))
				.Returns<string>(username => _mockUsers
					.FirstOrDefault(i => i.Username == username));

			offerRepositoryMock.Setup(i => i.Add(It.IsAny<Offer>()))
				.Callback<Offer>(i =>
				{
					i.UserId = i.User.Id;
					_mockOffers.Add(i);
				});

			_unitOfWorkMock.Setup(i => i.AuctionRepository).Returns(auctionRepositoryMock.Object);
			_unitOfWorkMock.Setup(i => i.UserRepository).Returns(userRepositoryMock.Object);
			_unitOfWorkMock.Setup(i => i.OfferRepository).Returns(offerRepositoryMock.Object);
		}

		private static Auction CreateMockAuction(string username)
		{
			return CreateMockAuction(username, 0, Guid.NewGuid().ToString());
		}		

		private static Auction CreateMockAuction(string username, int id, string description)
		{
			return CreateMockAuction(username, id, description, DateTime.Now.AddDays(1));
		}

		private static Auction CreateMockAuction(string username, int id, string description, DateTime endDate)
		{
			return new Auction
			{
				Id = id,
				Description = description,
				EndDate = endDate,
				User = new User
				{
					Username = username
				},
				Offers = new List<Offer>()
			};
		}

		private static User CreateMockUser(string username, int id)
		{
			return new User
			{
				Id = id,
				Username = username
			};
		}
	}
}
