﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FluentAssertions;
using Moq;
using OddamZaFlaszke.Api.Models;
using OddamZaFlaszke.Api.Services;
using Xunit;
using OddamZaFlaszke.Repository.Repositories;
using OddamZaFlaszke.Repository.Entities;
using OddamZaFlaszke.Services;

namespace OddamZaFlaszke.Api.Tests
{
	public class AuthenticationServiceTest
	{
		private readonly IAuthenticationService _authService;
		private readonly IPasswordService _passwordService;
		private Mock<IUserRepository> _userRepositoryMock;
		private List<User> _mockUsers;

		public AuthenticationServiceTest()
		{
			PrepareRepositoryMock();
			_passwordService = new PasswordService();
			_authService = new AuthenticationService(
				_userRepositoryMock.Object,
				_passwordService);
		}

		[Fact]
		public void ExistingUserIsValidatedWithSuccess()
		{
			//Arrange
			
			//Act

			//Assert
		}

		[Fact]
		public void NonExistingUserIsNotValidatedWithSuccess()
		{
			//Arrange

			//Act

			//Assert
		}

		[Fact]
		public void NewUserCanBeRegistered()
		{
			//Arrange

			//Act

			//Assert
		}

		private void PrepareRepositoryMock()
		{			
			_mockUsers = new List<User>();
			
			_userRepositoryMock = new Mock<IUserRepository>();

			_userRepositoryMock.Setup(i => i.Find(It.IsAny<string>()))
				.Returns<string>(username => _mockUsers
					.FirstOrDefault(i => i.Username == username));

			_userRepositoryMock.Setup(i => i.Add(It.IsAny<User>()))
				.Callback<User>(i =>
				{					
					_mockUsers.Add(i);
				});
		}

		private static User CreateMockUser(int id, string username, string password)
		{
			return new User
			{
				Id = id,
				Username = username,
				Password = password
			};
		}
	}
}
