namespace OddamZaFlaszke.Repository.Migrations
{
	using System.Data.Entity.Migrations;
    
    public partial class InitialSchemaAndData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Auction",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 2000),
                        CreationDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        CloseDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Offer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        AuctionId = c.Int(nullable: false),
                        CurrencyId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        Selected = c.Boolean(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Auction", t => t.AuctionId)
                .ForeignKey("dbo.Currency", t => t.CurrencyId)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.AuctionId)
                .Index(t => t.CurrencyId);
            
            CreateTable(
                "dbo.Currency",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false, maxLength: 50),
                        Password = c.String(nullable: false, maxLength: 255),
                        Email = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Username, unique: true)
                .Index(t => t.Email, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Auction", "UserId", "dbo.User");
            DropForeignKey("dbo.Offer", "UserId", "dbo.User");
            DropForeignKey("dbo.Offer", "CurrencyId", "dbo.Currency");
            DropForeignKey("dbo.Offer", "AuctionId", "dbo.Auction");
            DropIndex("dbo.User", new[] { "Email" });
            DropIndex("dbo.User", new[] { "Username" });
            DropIndex("dbo.Offer", new[] { "CurrencyId" });
            DropIndex("dbo.Offer", new[] { "AuctionId" });
            DropIndex("dbo.Offer", new[] { "UserId" });
            DropIndex("dbo.Auction", new[] { "UserId" });
            DropTable("dbo.User");
            DropTable("dbo.Currency");
            DropTable("dbo.Offer");
            DropTable("dbo.Auction");
        }
    }
}
