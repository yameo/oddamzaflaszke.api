using OddamZaFlaszke.Repository.Entities;
using OddamZaFlaszke.Repository.Enums;
using OddamZaFlaszke.Services;

namespace OddamZaFlaszke.Repository.Migrations
{
	using System.Data.Entity.Migrations;

	internal sealed class Configuration : DbMigrationsConfiguration<EntityFramework.RepositoryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EntityFramework.RepositoryContext context)
        {
			//Dictionary data
			context.Currencies.AddOrUpdate(
				i => i.Name,
				new Currency { Id = (int)CurrencyType.Beer, Name = CurrencyType.Beer.ToString() },
				new Currency { Id = (int)CurrencyType.Vodka, Name = CurrencyType.Vodka.ToString() },
				new Currency { Id = (int)CurrencyType.Wine, Name = CurrencyType.Wine.ToString() });

			//Test data
	        var passwordService = new PasswordService();

			context.Users.AddOrUpdate(
				i => i.Email,
				new User { Email = "user1@yameo.pl", Username = "user1", Password = passwordService.EncryptPassword("user1") },
				new User { Email = "user2@yameo.pl", Username = "user2", Password = passwordService.EncryptPassword("user2") });
        }
    }
}
