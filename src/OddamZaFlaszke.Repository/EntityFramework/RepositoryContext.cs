﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using OddamZaFlaszke.Repository.Entities;

namespace OddamZaFlaszke.Repository.EntityFramework
{
	public class RepositoryContext: DbContext
	{
		public RepositoryContext() : base("OddamZaFlaszkeCs")
		{
		}

		public DbSet<User> Users { get; set; }

		public DbSet<Auction> Auctions { get; set; }

		public DbSet<Offer> Offers { get; set; }

		public DbSet<Currency> Currencies { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
		}
	}
}
