﻿using System.Linq;
using OddamZaFlaszke.Repository.Entities;
using OddamZaFlaszke.Repository.EntityFramework;

namespace OddamZaFlaszke.Repository.Repositories
{
	public class OfferRepository : EntityRepository<Offer>, IOfferRepository
	{
		public OfferRepository()
		{
		}

		internal OfferRepository(RepositoryContext context)
			: base(context)
		{
		}
	}
}
