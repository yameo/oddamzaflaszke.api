﻿using OddamZaFlaszke.Repository.Entities;

namespace OddamZaFlaszke.Repository.Repositories
{
	public interface IUserRepository : IEntityRepository<User>
	{
		User Find(string username);
	}
}