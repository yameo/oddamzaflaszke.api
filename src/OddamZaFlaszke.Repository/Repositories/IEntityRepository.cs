﻿using System;
using System.Linq;
using System.Linq.Expressions;
using OddamZaFlaszke.Repository.Entities;

namespace OddamZaFlaszke.Repository.Repositories
{
	public interface IEntityRepository<TEntity> where TEntity : class, IEntity
	{
		void Add(TEntity entity);

		void Commit();

		void Dispose();

		TEntity Find(int id, params Expression<Func<TEntity, object>>[] include);

		IQueryable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] include);
	}
}