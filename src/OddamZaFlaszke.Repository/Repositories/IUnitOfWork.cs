﻿namespace OddamZaFlaszke.Repository.Repositories
{
	public interface IUnitOfWork
	{
		IUserRepository UserRepository { get; }

		IAuctionRepository AuctionRepository { get; }

		IOfferRepository OfferRepository { get; }

		void Commit();
	}
}