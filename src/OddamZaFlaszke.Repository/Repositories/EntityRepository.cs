﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using OddamZaFlaszke.Repository.Entities;
using OddamZaFlaszke.Repository.EntityFramework;

namespace OddamZaFlaszke.Repository.Repositories
{
	public abstract class EntityRepository<TEntity> : IDisposable, IEntityRepository<TEntity> where TEntity : class, IEntity
	{
		private bool _disposed;

		protected DbSet<TEntity> EntitySet
			=> Context.Set<TEntity>();

		protected RepositoryContext Context { get; private set; }

		protected EntityRepository()
			: this(new RepositoryContext())
		{
		}

		protected EntityRepository(RepositoryContext context)
		{
			Context = context;
		}

		public virtual TEntity Find(int id, params Expression<Func<TEntity, object>>[] include)
		{
			var entitySet = ApplyIncludesQuery(include);

			return entitySet.FirstOrDefault(i => i.Id == id);
		}

		public virtual IQueryable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] include)
		{
			var entitySet = ApplyIncludesQuery(include);

			return entitySet;
		}

		public virtual void Add(TEntity entity)
		{
			EntitySet.Add(entity);
		}

		public void Commit()
		{
			Context?.SaveChanges();
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private IQueryable<TEntity> ApplyIncludesQuery(params Expression<Func<TEntity, object>>[] include)
		{
			var entitySet = EntitySet.AsQueryable();
			include.Aggregate(entitySet, (current, t) => current.Include(t));

			return entitySet;
		}

		private void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing && Context != null)
				{
					Context.Dispose();
					Context = null;
				}
			}
			_disposed = true;
		}
	}
}
