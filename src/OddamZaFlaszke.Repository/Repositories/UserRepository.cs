﻿using System.Linq;
using OddamZaFlaszke.Repository.Entities;
using OddamZaFlaszke.Repository.EntityFramework;

namespace OddamZaFlaszke.Repository.Repositories
{
	public class UserRepository : EntityRepository<User>, IUserRepository
	{
		public UserRepository()
		{
		}

		internal UserRepository(RepositoryContext context)
			: base(context)
		{
		}

		public User Find(string username)
		{
			return EntitySet.FirstOrDefault(i => i.Username == username);
		}
	}
}
