﻿using System.Linq;
using OddamZaFlaszke.Repository.Entities;

namespace OddamZaFlaszke.Repository.Repositories
{
	public interface IAuctionRepository : IEntityRepository<Auction>
	{
		IQueryable<Auction> Find(string username);
	}
}