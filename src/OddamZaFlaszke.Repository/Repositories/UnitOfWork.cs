﻿using System;
using OddamZaFlaszke.Repository.EntityFramework;

namespace OddamZaFlaszke.Repository.Repositories
{
	public class UnitOfWork : IUnitOfWork, IDisposable
	{
		private bool _disposed;
		private RepositoryContext _context;

		public UnitOfWork()			
		{
			_context = new RepositoryContext();
		}

		private IUserRepository _userRepository;
		private IAuctionRepository _auctionRepository;
		private IOfferRepository _offerRepository;

		public IUserRepository UserRepository
			=> _userRepository ?? (_userRepository = new UserRepository(_context));

		public IAuctionRepository AuctionRepository
			=> _auctionRepository ?? (_auctionRepository = new AuctionRepository(_context));

		public IOfferRepository OfferRepository
			=> _offerRepository ?? (_offerRepository = new OfferRepository(_context));

		public void Commit()
		{
			_context.SaveChanges();
		}		

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing && _context != null)
				{
					_context.Dispose();
					_context = null;
				}
			}
			_disposed = true;
		}
	}
}
