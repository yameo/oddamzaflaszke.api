﻿using OddamZaFlaszke.Repository.Entities;

namespace OddamZaFlaszke.Repository.Repositories
{
	public interface IOfferRepository : IEntityRepository<Offer>
	{
	}
}