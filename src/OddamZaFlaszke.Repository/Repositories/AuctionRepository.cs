﻿using System.Linq;
using OddamZaFlaszke.Repository.Entities;
using OddamZaFlaszke.Repository.EntityFramework;

namespace OddamZaFlaszke.Repository.Repositories
{
	public class AuctionRepository : EntityRepository<Auction>, IAuctionRepository
	{
		public AuctionRepository()
		{
		}

		internal AuctionRepository(RepositoryContext context)
			: base(context)
		{
		}

		public IQueryable<Auction> Find(string username)
		{
			return EntitySet.Where(i => i.User.Username == username);
		}
	}
}
