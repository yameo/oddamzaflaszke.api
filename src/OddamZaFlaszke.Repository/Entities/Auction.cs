﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OddamZaFlaszke.Repository.Entities
{
	public class Auction : IEntity
	{
		public int Id { get; set; }

		[Required]
		public int UserId { get; set; }

		[Required]
		[StringLength(2000)]
		public string Description { get; set; }

		[Required]
		public DateTime CreationDate { get; set; }

		[Required]
		public DateTime EndDate { get; set; }

		public DateTime? CloseDate { get; set; }

		public virtual User User { get; set; }

		public virtual ICollection<Offer> Offers { get; set; }
	}
}
