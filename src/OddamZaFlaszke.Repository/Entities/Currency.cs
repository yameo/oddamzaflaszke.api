﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OddamZaFlaszke.Repository.Entities
{
	public class Currency : IEntity
	{
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public int Id { get; set; }

		[Required]
		public string Name { get; set; }
	}
}
