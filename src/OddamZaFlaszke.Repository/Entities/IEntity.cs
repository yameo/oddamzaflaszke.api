﻿namespace OddamZaFlaszke.Repository.Entities
{
	public interface IEntity
	{
		int Id { get; set; }
	}
}
