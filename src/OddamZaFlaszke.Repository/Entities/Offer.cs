﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OddamZaFlaszke.Repository.Enums;

namespace OddamZaFlaszke.Repository.Entities
{
	public class Offer : IEntity
	{
		public int Id { get; set; }

		[Required]
		public int UserId { get; set; }

		[Required]
		public int AuctionId { get; set; }

		[Required]
		public int CurrencyId { get; set; }

		[Required]
		public int Quantity { get; set; }

		[Required]
		public DateTime CreationDate { get; set; }

		[Required]
		public bool Selected { get; set; }

		public string Comment { get; set; }

		[NotMapped]
		public CurrencyType CurrencyType
		{
			get => (CurrencyType)CurrencyId;
			set => CurrencyId = (int) value;
		}
		
		public virtual User User { get; set; }

		public virtual Auction Auction { get; set; }

		public virtual Currency Currency { get; set; }
	}
}
