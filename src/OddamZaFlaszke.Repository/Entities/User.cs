﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OddamZaFlaszke.Repository.Entities
{
	public class User : IEntity
	{
		public int Id { get; set; }

		[Required]
		[StringLength(50)]
		[Index(IsUnique = true)]
		public string Username { get; set; }

		[Required]
		[StringLength(255)]
		public string Password { get; set; }

		[Required]
		[StringLength(255)]
		[Index(IsUnique = true)]
		public string Email { get; set; }
	}
}
