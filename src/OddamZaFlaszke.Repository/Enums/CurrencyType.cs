﻿namespace OddamZaFlaszke.Repository.Enums
{
	public enum CurrencyType
	{
		Beer = 1,

		Vodka = 2,

		Wine = 3
	}
}
