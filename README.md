# OddamZaFlaszke.Api

![N|Solid](http://yameo.pl/wp-content/uploads/2016/01/yameo_logo.png)

## How to start
Project uses SQL Server 2016 Express by defulat
Create DB using EntityFramework migrations process (recommended for development stage):
  - Open Package Manager Console
  - Run following command:
```sh
Update-Database -ProjectName OddamZaFlaszke.Repository
```
 
or create DB using powershell script:
  - Go to 'db' folder
  - Run proper PowerShell script depending on what SQL Server is installed on dev machine e.g.  for SQL Server 2016 Express run following script in PowerShell window
```sh
cd [solution folder]\db
.\CreateDB_SqlServer2016.ps1
```

## How to test API

### API documentation
API is documented by using Swagger library. Go to `/docs/index` page to see the documentation.

### API Authentication
A user must be authenticated before consumig API methods.
API uses OAuth2 standard for authentication purpose.
Use `oauth/token` endpoint to request for an OAuth2 token.

#### Request example
```sh
POST /oauth/token HTTP/1.1
Content-Type: application/x-www-form-urlencoded
Cache-Control: no-cache

username=user1&password=user1&grant_type=password
```

#### Response example
```sh
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8

{
"access_token": "aLJFaRRTlGAOtWc4x6NPFsHfs1qliJqZigZU17rh_...",
"token_type": "bearer",
"expires_in": 86399,
"userName": "user1",
".issued": "Thu, 24 Nov 2016 09:12:11 GMT",
".expires": "Fri, 25 Nov 2016 09:12:11 GMT"
}
```

Next each API request must include Authorization HTTP header to authorize a resource access.
```sh
Authorization: Bearer aLJFaRRTlGAOtWc4x6NPFsHfs1qliJqZigZU17rh_... 
```

### Recommended testing tools
- Use build-in Swagger documentation/testing tool
- Use Postman tool (https://www.getpostman.com/)